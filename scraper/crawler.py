import argparse
from pathlib import Path
from urllib.parse import urlparse, unquote

from bs4 import BeautifulSoup
import requests


def download(url):
    team = unquote(url.path.split("/")[-1])
    team_dir = Path("teams") / Path(team)
    team_dir.mkdir(parents=True, exist_ok=True)
    html = requests.get(url.geturl()).content
    with open(team_dir / "team.html", "wb") as fp:
        fp.write(html)
    soup = BeautifulSoup(html, 'html.parser')
    players = soup.select("div.player > .link-player")
    for i, player in enumerate(players):
        player_url = f"{url.scheme}://{url.hostname}/{player.attrs['href'].strip('/')}"
        player_name = unquote(player_url.split('/')[-1])
        with open(team_dir / f"{str(i).zfill(2)}-{player_name}.html", "wb") as fp:
            fp.write(requests.get(player_url).content)


def main():
    parser = argparse.ArgumentParser(description="Downloads teams and players from a list of team URLs")
    parser.add_argument("urls", help="File with a list of urls, one per line")

    args = parser.parse_args()

    with open(args.urls) as fp:
        for url in fp:
            download(urlparse(url.strip().rstrip("/")))


if __name__ == "__main__":
    main()
