from io import StringIO
from pathlib import Path

import yaml
from bs4 import BeautifulSoup
from lxml import etree


class Stats:
    ALL = {
        "Ball Control",
        "Dribbling",
        "Marking",
        "Slide Tackle",
        "Stand Tackle",
        "Aggression",
        "Reactions",
        "Att. Position",
        "Interceptions",
        "Vision",
        #"Composure",  not all players have this
        "Crossing",
        "Short Pass",
        "Long Pass",
        "Acceleration",
        "Stamina",
        "Strength",
        "Balance",
        "Sprint Speed",
        "Agility",
        "Jumping",
        "Heading",
        "Shot Power",
        "Finishing",
        "Long Shots",
        "Curve",
        "FK Acc.",
        "Penalties",
        "Volleys",
        "GK Positioning",
        "GK Diving",
        "GK Handling",
        "GK Kicking",
        "GK Reflexes",
    }
    def __init__(self):
        self._stats = {}

    def add_stat(self, name, value):
        if name in Stats.ALL:
            try:
                self._stats[name] = int(value)
            except TypeError:
                pass

    def asdict(self):
        if set(self._stats.keys()) != Stats.ALL:
            raise ValueError(f"Missing stats: {Stats.ALL - set(self._stats.keys())}")
        return self._stats


def read_name(pf):
    parser = etree.HTMLParser(encoding='utf-8')
    tree = etree.parse(str(pf), parser)
    return tree.xpath("/html/body/main/div/div[2]/div[2]/div[2]/div[2]/div/h5")[0].text


def read_stats(pf):
    with open(pf) as fp:
        soup = BeautifulSoup(fp.read(), 'html.parser')
    stats = Stats()
    for card in soup.select(".card-body"):
        for sub in card:
            if hasattr(sub, "children"):
                children = list(sub.children)
                ratings = sub.select(".rating")
                if ratings:
                    stats.add_stat(children[0].strip(), ratings[0].text)
    return stats.asdict()


def read_player(pf):
    player = {"name": read_name(pf)}
    player.update(read_stats(pf))
    return player


def fifa2funball(player):
    gk = [stat for name, stat in player.items() if name.startswith("GK")]
    return {
        "name": player["name"],
        "dribbling": round((player["Dribbling"] +  player["Ball Control"]) / 2),
        "marking": (player["Marking"] + player["Interceptions"]) / 2,
        "tackling": round((player["Slide Tackle"] + player["Stand Tackle"]) / 2),
        "passing": round((player["Short Pass"] + player["Long Pass"]) / 2),
        "shooting": round((player["Finishing"] + player["Long Shots"]) / 2),
        "speed": player["Sprint Speed"],
        "catching": round(sum(gk) / len(gk))
    }


def main():
    teams_dir = Path("teams")
    teams = []
    for team_dir in teams_dir.iterdir():
        team = {"name": team_dir.name, "goalkeepers": [], "outfielders": []}
        for player_file in sorted(team_dir.iterdir()):
            if player_file.name.startswith("team"):
                continue
            pos, _ = player_file.name.split("-", 1)
            player = fifa2funball(read_player(player_file))
            if pos == "00":
                team["goalkeepers"].append(player)
            else:
                team["outfielders"].append(player)
        teams.append(team)
    with open("teams.yaml", "w") as fp:
        yaml.dump_all(teams, fp)


if __name__ == "__main__":
    main()
