from funball import loader, core


def test_formation_to_positions():
    positions = loader.formation_to_positions("4-4-2")

    assert list(positions) == [
        core.Position(x=15, y=12),
        core.Position(x=15, y=24),
        core.Position(x=15, y=36),
        core.Position(x=15, y=48),
        core.Position(x=40, y=12),
        core.Position(x=40, y=24),
        core.Position(x=40, y=36),
        core.Position(x=40, y=48),
        core.Position(x=65, y=20),
        core.Position(x=65, y=40),
    ]
