from funball.main import play_league


def test_match(unbeatable_atletico):
    results = play_league(unbeatable_atletico)

    assert results[1].split(",")[0] == "Atlético Mineiro"
