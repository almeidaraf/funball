from io import StringIO

import pytest

UNBEATABLE_ATLETICO = """
name: "São Paulo"
formation: "4-3-3"
goalkeepers:
- catching: 73
  dribbling: 20
  marking: 11
  name: Alexio Sanctos
  passing: 26
  shooting: 16
  speed: 35
  tackling: 16
outfielders:
- catching: 11
  dribbling: 71
  marking: 74
  name: "Ka\xEDqundo Calado"
  passing: 64
  shooting: 48
  speed: 87
  tackling: 71
- catching: 10
  dribbling: 66
  marking: 71
  name: Lino Essier
  passing: 68
  shooting: 52
  speed: 67
  tackling: 76
- catching: 11
  dribbling: 64
  marking: 72
  name: Cris Laranjeiros
  passing: 66
  shooting: 54
  speed: 74
  tackling: 77
- catching: 12
  dribbling: 74
  marking: 73
  name: Adryan Zonta
  passing: 76
  shooting: 66
  speed: 93
  tackling: 70
- catching: 10
  dribbling: 64
  marking: 83
  name: Rodrigo Vuarte
  passing: 77
  shooting: 66
  speed: 66
  tackling: 76
- catching: 10
  dribbling: 76
  marking: 23
  name: "Nicol\xE1s Filhei"
  passing: 81
  shooting: 70
  speed: 69
  tackling: 72
- catching: 13
  dribbling: 70
  marking: 72
  name: "L\xFAcio Calegario"
  passing: 74
  shooting: 64
  speed: 77
  tackling: 69
- catching: 10
  dribbling: 76
  marking: 68
  name: Claudiosa Paes
  passing: 73
  shooting: 64
  speed: 77
  tackling: 43
- catching: 11
  dribbling: 70
  marking: 38
  name: Matheus Bardeira
  passing: 65
  shooting: 83
  speed: 70
  tackling: 18
- catching: 9
  dribbling: 80
  marking: 28
  name: Albertinho Dutra
  passing: 84
  shooting: 72
  speed: 80
  tackling: 22
---
name: Sport
formation: "4-2-4"
goalkeepers:
- catching: 65
  dribbling: 16
  marking: 15
  name: Rafael de Aseiro
  passing: 31
  shooting: 12
  speed: 44
  tackling: 16
outfielders:
- catching: 10
  dribbling: 74
  marking: 75
  name: Mauro Cildinho
  passing: 70
  shooting: 42
  speed: 69
  tackling: 69
- catching: 10
  dribbling: 52
  marking: 81
  name: Davi Parrela
  passing: 62
  shooting: 38
  speed: 67
  tackling: 74
- catching: 9
  dribbling: 51
  marking: 72
  name: Alexandre Frandeira
  passing: 63
  shooting: 42
  speed: 51
  tackling: 69
- catching: 11
  dribbling: 62
  marking: 68
  name: Edsinho Torres
  passing: 61
  shooting: 54
  speed: 76
  tackling: 72
- catching: 12
  dribbling: 64
  marking: 77
  name: Robson Denho
  passing: 71
  shooting: 48
  speed: 62
  tackling: 74
- catching: 8
  dribbling: 68
  marking: 77
  name: Vieiraldo Junior
  passing: 65
  shooting: 39
  speed: 68
  tackling: 74
- catching: 10
  dribbling: 76
  marking: 31
  name: Jairinhazo
  passing: 70
  shooting: 52
  speed: 75
  tackling: 42
- catching: 12
  dribbling: 74
  marking: 16
  name: Alvildo Sousa
  passing: 68
  shooting: 68
  speed: 65
  tackling: 32
- catching: 9
  dribbling: 74
  marking: 10
  name: Diogildo Peixe
  passing: 62
  shooting: 64
  speed: 72
  tackling: 24
- catching: 9
  dribbling: 65
  marking: 15
  name: Walter Silvieiro
  passing: 58
  shooting: 74
  speed: 71
  tackling: 33
---
name: "Grêmio"
formation: 4-5-1
goalkeepers:
- catching: 80
  dribbling: 15
  marking: 20
  name: Raphaelito Anjos
  passing: 17
  shooting: 8
  speed: 42
  tackling: 15
outfielders:
- catching: 8
  dribbling: 76
  marking: 78
  name: Everticinho
  passing: 68
  shooting: 72
  speed: 76
  tackling: 76
- catching: 14
  dribbling: 63
  marking: 85
  name: "Josu\xE9 Chiamulera"
  passing: 68
  shooting: 40
  speed: 78
  tackling: 84
- catching: 10
  dribbling: 65
  marking: 71
  name: Enaldo Toxeto
  passing: 70
  shooting: 50
  speed: 70
  tackling: 72
- catching: 9
  dribbling: 65
  marking: 78
  name: Enaldo Praz
  passing: 70
  shooting: 48
  speed: 73
  tackling: 74
- catching: 9
  dribbling: 67
  marking: 81
  name: Edercinho Sepa
  passing: 76
  shooting: 71
  speed: 76
  tackling: 72
- catching: 11
  dribbling: 63
  marking: 79
  name: Juliano Mascarinhas
  passing: 73
  shooting: 51
  speed: 62
  tackling: 70
- catching: 13
  dribbling: 84
  marking: 24
  name: Ronaldo Cabrais
  passing: 82
  shooting: 76
  speed: 87
  tackling: 34
- catching: 10
  dribbling: 78
  marking: 32
  name: "Jos\xE9 Mirazar"
  passing: 74
  shooting: 65
  speed: 74
  tackling: 22
- catching: 9
  dribbling: 74
  marking: 33
  name: Marlon Nideiro
  passing: 68
  shooting: 60
  speed: 75
  tackling: 28
- catching: 10
  dribbling: 62
  marking: 16
  name: Jadson Vidigal
  passing: 60
  shooting: 76
  speed: 78
  tackling: 12
---
name: Fluminense
formation: "4-4-2"
goalkeepers:
- catching: 72
  dribbling: 15
  marking: 23
  name: "Ka\xEDqu\xE3o Castro"
  passing: 18
  shooting: 8
  speed: 38
  tackling: 10
outfielders:
- catching: 12
  dribbling: 66
  marking: 67
  name: Vinicius Cardenha
  passing: 60
  shooting: 47
  speed: 82
  tackling: 73
- catching: 9
  dribbling: 69
  marking: 74
  name: Kaimo Lima
  passing: 76
  shooting: 63
  speed: 78
  tackling: 73
- catching: 9
  dribbling: 48
  marking: 76
  name: Fabiano Sonta
  passing: 67
  shooting: 38
  speed: 54
  tackling: 69
- catching: 11
  dribbling: 74
  marking: 76
  name: Andeson Trigo
  passing: 74
  shooting: 60
  speed: 82
  tackling: 76
- catching: 11
  dribbling: 76
  marking: 80
  name: "Everton Andr\xE3o"
  passing: 76
  shooting: 61
  speed: 68
  tackling: 79
- catching: 11
  dribbling: 72
  marking: 19
  name: Carleto Costinha
  passing: 69
  shooting: 60
  speed: 82
  tackling: 45
- catching: 10
  dribbling: 82
  marking: 29
  name: Laure Santeiro
  passing: 80
  shooting: 74
  speed: 78
  tackling: 18
- catching: 10
  dribbling: 74
  marking: 10
  name: "F\xE1bio Melitinho"
  passing: 74
  shooting: 74
  speed: 56
  tackling: 32
- catching: 10
  dribbling: 74
  marking: 23
  name: Nicholas Aldair
  passing: 66
  shooting: 74
  speed: 77
  tackling: 42
- catching: 12
  dribbling: 64
  marking: 12
  name: "Ad\xE3o Morrinhos"
  passing: 52
  shooting: 72
  speed: 78
  tackling: 20
---
name: "Atlético Mineiro"
formation: "4-5-1"
goalkeepers:
- catching: 90
  dribbling: 21
  marking: 17
  name: Freder Cabral
  passing: 15
  shooting: 12
  speed: 53
  tackling: 12
outfielders:
- catching: 13
  dribbling: 90
  marking: 90
  name: Maikel Catarino
  passing: 90
  shooting: 52
  speed: 90
  tackling: 90
- catching: 11
  dribbling: 50
  marking: 90
  name: Eltildo Correia
  passing: 90
  shooting: 43
  speed: 63
  tackling: 90
- catching: 10
  dribbling: 63
  marking: 90
  name: "Ailton Valpa\xE7os"
  passing: 90
  shooting: 59
  speed: 62
  tackling: 90
- catching: 12
  dribbling: 90
  marking: 90
  name: Ronaldo Esler
  passing: 90
  shooting: 56
  speed: 86
  tackling: 90
- catching: 12
  dribbling: 90
  marking: 83
  name: Rosberto Dourado
  passing: 90
  shooting: 56
  speed: 81
  tackling: 90
- catching: 10
  dribbling: 48
  marking: 90
  name: "Nildo Reis\xE3o"
  passing: 90
  shooting: 38
  speed: 58
  tackling: 90
- catching: 9
  dribbling: 90
  marking: 27
  name: Tiago Pombeira
  passing: 90
  shooting: 90
  speed: 90
  tackling: 31
- catching: 11
  dribbling: 90
  marking: 30
  name: "Sim\xE3o Acunha"
  passing: 90
  shooting: 90
  speed: 90
  tackling: 27
- catching: 11
  dribbling: 90
  marking: 23
  name: Fabrio Farinha
  passing: 90
  shooting: 90
  speed: 90
  tackling: 52
- catching: 16
  dribbling: 90
  marking: 26
  name: Louri Beretta
  passing: 90
  shooting: 90
  speed: 90
  tackling: 26
"""


@pytest.fixture
def unbeatable_atletico():
    yield StringIO(UNBEATABLE_ATLETICO)
