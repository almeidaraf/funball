from setuptools import find_packages, setup

setup(
    name="funball",
    version="0.0.0",
    install_requires=["numpy>=1.18.5", "pyyaml>=5.3.1"],
    extras_require={"tests": ["pytest>=5.4.3"]},
    packages=find_packages(),
)
