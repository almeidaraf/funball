import copy
import json
import math
from enum import Enum
from typing import NamedTuple, List, Tuple, Iterable

FIELD_WIDTH: int = 60
FIELD_LENGTH: int = 100

MAX_DISTANCE: float = math.sqrt((FIELD_WIDTH) ** 2 + (FIELD_LENGTH) ** 2)

MAX_SKILL: float = 100.0

ACTIONS_PER_MINUTE: int = 60
MAX_SPEED_MPS: float = 10


class Position(NamedTuple):
    x: float
    y: float

    def __str__(self) -> str:
        return f"<{self.x},{self.y}>"

    def __repr__(self) -> str:
        return str(self)


class Player(NamedTuple):
    name: str
    # offense scores
    dribbling: float
    passing: float
    shooting: float
    speed: float
    # defense scores
    tackling: float
    marking: float
    catching: float

    def __str__(self) -> str:
        return self.name

    def __repr__(self) -> str:
        return str(self)


class Team(NamedTuple):
    name: str
    formation: str
    colour: str
    outfielders: Tuple[Tuple[Player, Position], ...]
    goalkeepers: Tuple[Tuple[Player, Position], ...]

    def __str__(self) -> str:
        return self.name

    def __repr__(self) -> str:
        return str(self)


class ActionBuffer:
    def __init__(self) -> None:
        self._messages = []

    def add(self, output: dict) -> None:
        self._messages.append(copy.deepcopy(output))

    def asjson(self):
        return json.dumps({"actions": self._messages})


class Heading(Enum):
    LEFT = 1
    RIGHT = 2


def get_distance(pos1: Position, pos2: Position) -> float:
    return math.sqrt((pos1.x - pos2.x) ** 2 + (pos1.y - pos2.y) ** 2)


def distance_decay(pos1: Position, pos2: Position, prob: float, k: float) -> float:
    return prob * math.exp(k * get_distance(pos1, pos2))


def is_in_front(other: Position, me: Position, heading: Heading) -> bool:
    if heading == Heading.RIGHT:
        return other.x > me.x
    else:
        return other.x < me.x


def get_closest_player(player_pos: Position, team: Team) -> Player:
    return min(team.outfielders, key=lambda x: get_distance(player_pos, x[1]))[0]
