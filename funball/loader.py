from itertools import repeat
from typing import List, TextIO

import yaml

from .core import Player, Position, FIELD_LENGTH, FIELD_WIDTH, Team

DEFENSIVE_MOVEBACK = 10

COLOURS = [
    "#b5a1fd",
    "#a1e9fd",
    "#fdb5a1",
    "#221600",
    "#e4b2ab",
    "#a71d31",
    "#da4167",
    "#cd5334",
    "#06dda0",
    "#de9151",
    "#f34213",
    "#035e7b",
    "#f28123",
    "#f7f052",
    "#e54b4b",
    "#0f5257",
    "#3d5467",
    "#e4be9e",
    "#9e7b9b",
    "#003459",
    "#ffc6ac",
    "#eba6a9",
    "#5a6650",
    "#a0c4e2",
    "#ffbf81",
    "#ffdc5e",
    "#ddc3d0",
    "#ef7a85",
    "#011638",
    "#bf4458",
    "#cbcba9",
    "#a2eee3",
    "#5a4b5e",
]


def formation_to_positions(formation: str) -> List[Position]:
    positions: List[Position] = []

    areas: List[int] = list(map(int, formation.split("-")))
    x_gap = round(FIELD_LENGTH / (len(areas) + 1))
    for area, x in zip(areas, range(x_gap, FIELD_LENGTH, x_gap)):
        y_gap = round(FIELD_WIDTH / (area + 1))
        for y in range(y_gap, FIELD_WIDTH, y_gap):
            positions.append(Position(x=x - DEFENSIVE_MOVEBACK, y=y))

    return positions


def _convert_players(dplayers: List[dict]) -> List[Player]:
    players: List[Player] = []
    for dplayer in dplayers:
        players.append(
            Player(
                name=dplayer["name"],
                dribbling=dplayer["dribbling"] / 100,
                marking=dplayer["marking"] / 100,
                tackling=dplayer["tackling"] / 100,
                passing=dplayer["passing"] / 100,
                shooting=dplayer["shooting"] / 100,
                speed=dplayer["speed"] / 100,
                catching=dplayer["catching"] / 100,
            )
        )
    return players


def load_teams(fp: TextIO) -> List[Team]:
    teams: List[Team] = []

    for i, team in enumerate(yaml.safe_load_all(fp)):
        positions = formation_to_positions(team["formation"])
        outfielders = _convert_players(team["outfielders"])
        goalkeepers = _convert_players(team["goalkeepers"])
        teams.append(
            Team(
                name=team["name"],
                formation=team["formation"],
                colour=COLOURS[i],
                outfielders=tuple(zip(outfielders, positions)),
                goalkeepers=tuple(zip(goalkeepers, repeat(Position(x=0.0, y=FIELD_WIDTH / 2)))),
            )
        )

    return teams
