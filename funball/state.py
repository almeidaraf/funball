import json
import logging
import math
from typing import Dict, Tuple, Optional, List, Union

from typing.io import TextIO

from .core import (
    Team,
    Position,
    FIELD_LENGTH,
    FIELD_WIDTH,
    Player,
    ACTIONS_PER_MINUTE,
    MAX_SPEED_MPS,
    Heading,
    get_distance, ActionBuffer,
)

logger = logging.getLogger(__name__)


class OutOfBounds(Exception):
    pass


class Match:
    # immutable
    team_left: Team
    team_right: Team

    # mutable
    ball_carrier_position: Position
    current_team: Team
    opposing_team: Team
    score: Dict[str, int]

    def __init__(self, team_left: Team, team_right: Team, buffer: ActionBuffer, score: Optional[Dict[str, int]] = None) -> None:
        self.team_left = team_left
        self.team_right = self._flip_team(team_right)
        self.heading = Heading.RIGHT
        if score is not None:
            self.score = score
        else:
            self.score: Dict[str, int] = {team_left.name: 0, team_right.name: 0}
        self.buffer = buffer
        self._ball_carrier = None


    def start(self):
        self._log_beginning()
        self.set_ball_carrier(self.team_left.outfielders[int(len(self.team_left.outfielders) / 2)][0])
        self.move_ball_carrier(to=Position(FIELD_LENGTH / 2, FIELD_WIDTH / 2))

    def log(self, ball: Optional[Dict[str, float]], players: List[Dict[str, Union[str, float]]]):
        self.buffer.add({"ball": ball, "players": players, "score": self.score})

    def _player(self, player: Player, pos: Position, colour: str) -> Dict[str, Union[str, float]]:
        return {"name": player.name, "x": pos.x, "y": pos.y, "colour": colour}

    def _team(self, team: Team) -> List[Dict[str, Union[str, float]]]:
        players: List[Dict[str, Union[str, float]]] = []
        gk, gkpos = team.goalkeepers[0]
        players.append(self._player(gk, gkpos, team.colour))
        for (player, position) in team.outfielders:
            players.append(self._player(player, position, team.colour))
        return players

    def _log_beginning(self) -> None:
        players = self._team(self.team_left) + self._team(self.team_right)
        self.log(ball=None, players=players)

    def _flip_team(self, team: Team) -> Team:
        outfielders = []
        for player, position in team.outfielders:
            outfielders.append((player, Position(x=FIELD_LENGTH - position.x, y=position.y)))
        goalkeepers = []
        for player, position in team.goalkeepers:
            goalkeepers.append((player, Position(x=FIELD_LENGTH - position.x, y=position.y)))
        return Team(
            name=team.name, colour=team.colour, formation=team.formation, outfielders=tuple(outfielders), goalkeepers=tuple(goalkeepers)
        )

    def get_player_meta(self, needle: Player) -> Tuple[Team, Position]:
        for player, position in self.team_right.outfielders:
            if player.name == needle.name:
                return (self.team_right, position)
        for player, position in self.team_left.outfielders:
            if player.name == needle.name:
                return (self.team_left, position)
        raise KeyError(f"the layer {needle.name} is not playing")

    def get_other_team(self, team: Team) -> Team:
        return self.team_left if team.name != self.team_left.name else self.team_right

    @property
    def ball_carrier(self) -> Player:
        return self._ball_carrier

    def set_ball_carrier(self, new_player: Player) -> None:
        players = []
        if self._ball_carrier is not None:
            _, old_position = self.get_player_meta(self._ball_carrier)
            players.append(self._player(self._ball_carrier, old_position, self.current_team.colour))
        self._ball_carrier = new_player

        new_team, new_position = self.get_player_meta(new_player)
        self.ball_carrier_position = new_position
        players.append(self._player(new_player, new_position, new_team.colour))

        self.current_team = new_team
        self.opposing_team = self.get_other_team(new_team)
        self.heading = Heading.RIGHT if self.is_home_player(self._ball_carrier) else Heading.LEFT

        self.log(ball={"x": new_position.x, "y": new_position.y}, players=players)

    def move_ball_carrier(self, to: Optional[Position]=None) -> None:
        try:
            if to is not None:
                self.ball_carrier_position = to
            else:
                delta_t_sec = 60 / ACTIONS_PER_MINUTE
                max_meters = MAX_SPEED_MPS * delta_t_sec
                actual_meters = self.ball_carrier.speed * max_meters
                gk_pos = self.opposing_team.goalkeepers[0][1]
                distance = get_distance(self.ball_carrier_position, gk_pos)
                if distance >= 1:
                    delta_y = abs(self.ball_carrier_position.y - gk_pos.y) * (actual_meters / distance)
                else:
                    delta_y = 0
                delta_x = math.sqrt(actual_meters ** 2 - delta_y ** 2)
                if gk_pos.x > self.ball_carrier_position.x:
                    self.ball_carrier_position = Position(
                        x=self.ball_carrier_position.x + delta_x, y=self.ball_carrier_position.y + delta_y,
                    )
                else:
                    self.ball_carrier_position = Position(
                        x=self.ball_carrier_position.x - delta_x, y=self.ball_carrier_position.y - delta_y,
                    )
                if self.ball_carrier_position.y < 0.0 or self.ball_carrier_position.y > FIELD_LENGTH:
                    raise OutOfBounds
                if self.ball_carrier_position.x < 0.0 or self.ball_carrier_position.x > FIELD_WIDTH:
                    raise OutOfBounds
        finally:
            self.log(
                ball={"x": self.ball_carrier_position.x, "y": self.ball_carrier_position.y},
                players=[self._player(self.ball_carrier, self.ball_carrier_position, self.current_team.colour)],
            )

    def goal(self) -> None:
        team, _ = self.get_player_meta(self.ball_carrier)
        self.score[team.name] += 1
        self.set_ball_carrier(self.opposing_team.outfielders[int(len(self.opposing_team.outfielders) / 2)][0])

    def is_home_player(self, player: Player) -> bool:
        team, _ = self.get_player_meta(player)
        return team.name == self.team_left.name
