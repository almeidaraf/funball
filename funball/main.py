import logging
import sys
from typing import List, Dict, TextIO

import numpy as np  # type: ignore

from funball.core import ActionBuffer
from . import loader, core, state
from .actions import dribble, pass_, shot, noop

logger = logging.getLogger(__name__)


def simulate(match: state.Match, time_m: int) -> None:
    match.start()
    for s in range(time_m):
        prob_pass, f_pass = pass_.try_to_pass(
            match.ball_carrier, match.ball_carrier_position, match.heading, match.current_team, match.opposing_team,
        )
        prob_dribble, f_dribble = dribble.try_to_dribble(
            match.ball_carrier, match.ball_carrier_position, match.opposing_team,
        )
        prob_shoot, f_shoot = shot.try_to_shoot(
            match.ball_carrier, match.ball_carrier_position, match.heading, match.opposing_team,
        )
        prob_noop, f_noop = noop.try_to_do_nothing()

        # Usually chooses the safest bet
        probs = np.array((prob_pass, prob_dribble, prob_shoot, prob_noop))
        f = np.random.choice((f_pass, f_dribble, f_shoot, f_noop), 1, p=probs / probs.sum())[0]

        f(match)


def debug() -> None:
    with open("data/teams.yaml") as fp:
        teams: List[core.Team] = loader.load_teams(fp)

    scores: Dict[str, float] = {}

    logger.debug("First half")
    first_half = state.Match(team_left=teams[0], team_right=teams[1])
    simulate(first_half, 45 * core.ACTIONS_PER_MINUTE)
    logger.debug("Second Half")
    second_half = state.Match(team_left=teams[1], team_right=teams[0])
    simulate(second_half, 45 * core.ACTIONS_PER_MINUTE)

    for team, score in first_half.score.items():
        scores.setdefault(team, 0)
        scores[team] += second_half.score[team] + score

    print(scores)
    # plt.hist2d(fx, fy, bins=8, range=[[0, 100], [0, 60]], cmap='Reds')


def play_match(a: core.Team, b: core.Team, buffer: ActionBuffer) -> Dict[str, int]:
    first_half = state.Match(team_left=a, team_right=b, buffer=buffer)
    simulate(first_half, 45 * core.ACTIONS_PER_MINUTE)

    second_half = state.Match(team_left=b, team_right=a, buffer=buffer, score=first_half.score)
    simulate(second_half, 45 * core.ACTIONS_PER_MINUTE)

    return second_half.score


def update_board(team_a: str, team_b: str, score: Dict[str, int], board: Dict[str, Dict[str, int]]) -> None:
    stats = board[team_a]
    if score[team_a] > score[team_b]:
        stats["p"] += 3
        stats["v"] += 1
    elif score[team_a] == score[team_b]:
        stats["p"] += 1
        stats["t"] += 1
    else:
        stats["l"] += 1
    stats["gf"] += score[team_a]
    stats["ga"] += score[team_b]


def play_league(fp: TextIO) -> List[str]:
    teams: List[core.Team] = loader.load_teams(fp)

    board = {}
    for team in teams:
        board[team.name] = {"p": 0, "v": 0, "t": 0, "l": 0, "gf": 0, "ga": 0}
    for team_a in teams:
        for team_b in teams:
            if team_a.name == team_b.name:
                continue
            score = play_match(team_a, team_b, ActionBuffer())
            update_board(team_a.name, team_b.name, score, board)
            update_board(team_b.name, team_a.name, score, board)

    results = sorted(board.items(), key=lambda x: (x[1]["p"], x[1]["v"], x[1]["gf"]), reverse=True)
    rows = ["Time,P,V,E,D,GP,GC"]
    for name, result in results:
        row = [
            name,
            str(result["p"]),
            str(result["v"]),
            str(result["t"]),
            str(result["l"]),
            str(result["gf"]),
            str(result["ga"]),
        ]
        rows.append(",".join(row))
    return rows


def run() -> None:
    logging.basicConfig(
        stream=sys.stdout, level=logging.INFO, format="[%(asctime)s][%(levelname)s] %(message)s",
    )

    with open("data/brasileirao-2020.yaml") as fp:
        print("\n".join(play_league(fp)))


if __name__ == "__main__":
    run()
