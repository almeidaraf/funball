import logging
from functools import partial, lru_cache
from typing import Tuple, Callable, Optional

import numpy as np  # type: ignore

from ..core import Player, Position, Team, distance_decay, is_in_front, Heading
from ..state import Match

logger = logging.getLogger(__name__)

FORWARD_PASS_BOOST: float = 3.0
MARKING_DECAY: float = -0.15


def pass_it(success: bool, target: Player, interceptor: Optional[Player], match: Match) -> None:
    logger.debug("[%s, %s] tries to PASS to [%s]", match.ball_carrier, match.current_team, target)
    if success:
        match.set_ball_carrier(target)
    else:
        logger.debug("[%s, %s] INTERCEPTS it", interceptor, match.opposing_team)
        assert interceptor is not None
        match.set_ball_carrier(interceptor)


def try_to_pass(
    myself: Player, my_pos: Position, heading: Heading, own_team: Team, other_team: Team
) -> Tuple[float, Callable[[Match], None]]:
    target, intercept_probs = _find_target(
        my_pos, myself.passing, heading, _get_team_mates(myself, own_team.outfielders), other_team.outfielders,
    )

    intercepts = np.random.binomial(1, intercept_probs)
    success = intercepts.sum() == 0
    if success:
        interceptor = None
    else:
        interceptor = other_team.outfielders[intercepts.argmax()][0]

    return ((1 - intercept_probs).prod(), partial(pass_it, success, target[0], interceptor))


@lru_cache(maxsize=None)  # type: ignore
def _get_intercept_probs(
    my_passing: float, team_mates: Tuple[Tuple[Player, Position], ...], other_team: Tuple[Tuple[Player, Position], ...],
) -> np.ndarray:
    intercept_probs: np.ndarray = np.zeros((len(team_mates), len(other_team)))

    for i, (mate, mate_pos) in enumerate(team_mates):
        intercept_probs[i] = _markers_to_probs(mate_pos, other_team)

    return intercept_probs * (1 - my_passing)


def _find_target(
    my_pos: Position,
    my_passing: float,
    heading: Heading,
    team_mates: Tuple[Tuple[Player, Position], ...],
    other_team: Tuple[Tuple[Player, Position], ...],
) -> Tuple[Tuple[Player, Position], np.ndarray]:
    intercept_probs = _get_intercept_probs(my_passing, team_mates, other_team)

    # chance that all markers will fail
    success_probs = (1 - intercept_probs).prod(axis=1)

    # Boost forward passes
    in_front = [is_in_front(mate[1], my_pos, heading) for mate in team_mates]
    success_probs[in_front] = success_probs[in_front] * FORWARD_PASS_BOOST
    choice = np.random.choice(np.arange(len(success_probs)), 1, p=success_probs / success_probs.sum())

    return (team_mates[choice[0]], intercept_probs[choice[0]])


def _markers_to_probs(target: Position, players: Tuple[Tuple[Player, Position], ...]) -> np.ndarray:
    intercept_probs: np.ndarray = np.zeros(len(players))

    for i, (marker, marker_pos) in enumerate(players):
        intercept_probs[i] = distance_decay(marker_pos, target, marker.marking, k=MARKING_DECAY)

    return intercept_probs


def _get_team_mates(myself: Player, mates: Tuple[Tuple[Player, Position], ...]) -> Tuple[Tuple[Player, Position], ...]:
    return tuple(mate for mate in mates if mate[0] != myself)
