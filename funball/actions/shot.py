import logging
from functools import partial
from typing import Tuple, Callable, List

import numpy as np  # type: ignore

from funball.core import Player, Team, Position, get_closest_player, Heading, is_in_front, distance_decay
from funball.state import Match

logger = logging.getLogger(__name__)

SHOT_DECAY: float = -0.008
INTERCEPTING_DECAY: float = -0.07


def shoot_it(success: bool, defender: Player, match: Match) -> None:
    logger.debug("[%s, %s] SHOOTS from %s", match.ball_carrier, match.current_team, match.ball_carrier_position)
    if success:
        logger.debug("[%s] GOAL!", match.ball_carrier)
        match.goal()
    else:
        logger.debug("[%s] MISSED", match.ball_carrier)
        match.set_ball_carrier(defender)


def try_to_shoot(
    player: Player, own_pos: Position, heading: Heading, other_team: Team
) -> Tuple[float, Callable[[Match], None]]:
    probs: List[float] = []
    goalkeeper_pos = other_team.goalkeepers[0][1]
    for (other, other_pos) in other_team.outfielders:
        if is_in_front(other_pos, own_pos, heading):
            probs.append(distance_decay(other_pos, own_pos, other.marking, INTERCEPTING_DECAY))
    probs.append(other_team.goalkeepers[0][0].catching)

    success_prob = distance_decay(own_pos, goalkeeper_pos, player.shooting, SHOT_DECAY) * (1 - np.array(probs)).prod()
    success = bool(np.random.binomial(1, success_prob))

    f = partial(shoot_it, success, get_closest_player(other_team.goalkeepers[0][1], other_team))
    return (success_prob, f)
