from typing import Tuple, Callable, Match


def try_to_do_nothing() -> Tuple[float, Callable[[Match], None]]:
    def f(_: Match) -> None:
        pass

    return 15.0, f
