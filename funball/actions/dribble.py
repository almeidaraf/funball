import logging
from functools import partial
from typing import Callable, Tuple

import numpy as np  # type: ignore

from funball import state
from funball.core import Player, Position, Team, get_closest_player, distance_decay
from funball.state import Match

logger = logging.getLogger(__name__)

TACKLING_DECAY = -0.3


def dribble(success: bool, tackler: Player, match: Match) -> None:
    logger.debug("[%s, %s] is DRIBBLING", match.ball_carrier, match.current_team)
    if success:
        try:
            match.move_ball_carrier()
        except state.OutOfBounds:
            logger.debug("[%s, %s] went OUT OF BOUNDS", match.ball_carrier.name, match.current_team)
            match.set_ball_carrier(get_closest_player(match.ball_carrier_position, match.opposing_team))
    else:
        logger.debug("[%s, %s] TACKLES", tackler, match.opposing_team)
        match.set_ball_carrier(tackler)


def try_to_dribble(player: Player, own_pos: Position, other_team: Team) -> Tuple[float, Callable[[Match], None]]:
    tackle_probs: np.ndarray = np.zeros(len(other_team.outfielders))
    for i, (adversary, adversary_pos) in enumerate(other_team.outfielders):
        tackle_probs[i] = distance_decay(adversary_pos, own_pos, adversary.tackling, k=TACKLING_DECAY) * (
            1 - player.dribbling
        )

    success_prob = (1 - tackle_probs).prod()
    tackles = np.random.binomial(1, tackle_probs)
    success = tackles.sum() == 0
    if success:
        tackler = None
    else:
        tackler = other_team.outfielders[tackles.argmax()][0]

    return (success_prob, partial(dribble, success, tackler))
